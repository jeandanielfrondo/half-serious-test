import { Component, OnInit } from '@angular/core';
import { StarshipsService } from './services/starships-service/starships.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private _spaceshipService: StarshipsService) {}
  ngOnInit() {
    this._spaceshipService.fetchStarShips();
  }
}
