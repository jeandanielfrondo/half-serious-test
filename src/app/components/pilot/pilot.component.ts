import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { StarshipsStateService } from '../../states/starships-state.service';

@Component({
  selector: 'app-pilot',
  templateUrl: './pilot.component.html',
  styleUrls: ['./pilot.component.scss'],
})
export class PilotComponent implements OnInit, OnDestroy {
  pilot$: Observable<any>;
  routerParamSubscription: Subscription;
  constructor(
    private _route: Router,
    private _router: ActivatedRoute,
    private _store: Store
  ) {}

  ngOnDestroy(): void {
    if (this.routerParamSubscription) {
      this.routerParamSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.routerParamSubscription = this._router.params.subscribe((value) => {
      this.pilot$ = this._store.select(StarshipsStateService.getPilot).pipe(
        map((filter) =>
          filter(Number(value['spaceshipId']), Number(value['pilotId']))
        ),
      );
    });
  }

  back(): void {
    this._route.navigate(['home']);
  }
}
