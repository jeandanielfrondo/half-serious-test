import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { StarshipsStateService } from '../../states/starships-state.service';

@Component({
  selector: 'app-list-spaceships',
  templateUrl: './list-spaceships.component.html',
  styleUrls: ['./list-spaceships.component.scss']
})
export class ListSpaceshipsComponent implements OnInit {
  @Select(StarshipsStateService.getStarships) startships$: Observable<any>;
  searchForm : FormGroup;
  constructor() { }

  ngOnInit() {
    this.searchForm = new FormBuilder().group({
      search: new FormControl('', [Validators.required])
    }, {
      updateOn: 'change'
    });
  }

}
