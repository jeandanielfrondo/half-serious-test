import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-spaceship',
  templateUrl: './spaceship.component.html',
  styleUrls: ['./spaceship.component.scss']
})
export class SpaceshipComponent implements OnInit {
  @Input() image: string;
  @Input() name : string;
  @Input() capacity : number;
  @Input() atmosphereSpeed : number;
  @Input() starshipClass: string;
  @Input() manufacturer: string;
  @Input() costCredits: number;
  @Input() passengersNbr: number;
  @Input() model : string;
  @Input() pilots: any[];
  @Input() id: string;
  constructor() { }

  ngOnInit() {
  }

  isNumber(value : string | number) {
    return !!Number(value)
  }

}
