import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListSpaceshipsComponent } from './components/list-spaceships/list-spaceships.component';
import { StarshipsService } from './services/starships-service/starships.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { StarshipsStateService } from './states/starships-state.service';
import { SpaceshipComponent } from './components/spaceship/spaceship.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr-CA';
import { PilotComponent } from './components/pilot/pilot.component';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { StarshipsFilterPipe } from './pipes/starships-filter.pipe';

registerLocaleData(localeFr, 'fr-CA');
@NgModule({
  declarations: [
    AppComponent,
    ListSpaceshipsComponent,
    SpaceshipComponent,
    PilotComponent,
    StarshipsFilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    NgxsModule.forRoot([StarshipsStateService], {
      developmentMode: !environment.production,
    }),
    NgxsLoggerPluginModule.forRoot({
      disabled: environment.production,
    }),
  ],
  providers: [
    StarshipsService,
    //    {
    //   provide : APP_INITIALIZER,
    //   useFactory : () => {},
    //   deps: []
    // }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
