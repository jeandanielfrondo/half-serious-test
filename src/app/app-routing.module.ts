import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSpaceshipsComponent } from './components/list-spaceships/list-spaceships.component';
import { PilotComponent } from './components/pilot/pilot.component';

const routes: Routes = [
  {
    path: '',
    redirectTo : 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: ListSpaceshipsComponent
  }, {
    path: 'pilot/:spaceshipId/:pilotId',
    component: PilotComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
