/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StarshipsStateService } from './starships-state.service';

describe('Service: StarshipsState', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StarshipsStateService]
    });
  });

  it('should ...', inject([StarshipsStateService], (service: StarshipsStateService) => {
    expect(service).toBeTruthy();
  }));
});
