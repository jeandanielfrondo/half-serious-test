import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SetStarShips } from '../actions/starsips.actions';
@State<any>({
  name: 'starships',
  defaults: [],
})
@Injectable()
export class StarshipsStateService {
  constructor() {}

  @Selector()
  static getStarships(state: any) {
    return state;
  }

  @Selector()
  static getStarshipByIndex(index: number) {
    return (state: any) => {
      return state[index];
    };
  }

  @Selector()
  static getPilot(state: any) {
    return (spaceshipId: number, id: number) => {
      const pilot = state
        .find((spaceship: any) => spaceship.id === spaceshipId)
        .pilots.find((pilot: any) => pilot.id === id);
      return pilot;
    };
  }

  @Action(SetStarShips)
  public SetUserInfo(ctx: StateContext<any>, { starships }: SetStarShips) {
    ctx.setState(starships);
  }
}
