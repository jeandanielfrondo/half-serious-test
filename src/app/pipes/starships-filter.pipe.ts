import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'starshipsfilter',
})
export class StarshipsFilterPipe implements PipeTransform {
  transform(value: any[], args?: any): any {
    return !args ? value : value.filter((item) => this.predicate(item, args));
  }

  predicate(value: any, search: string) {
    return (
      value.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()) ||
      value.manufacturer.toLocaleLowerCase().includes(search) ||
      value.model.toLocaleLowerCase().includes(search) ||
      value.passengers.toLocaleLowerCase().includes(search) ||
      value.starship_class.toLocaleLowerCase().includes(search) ||
      value.model.toLocaleLowerCase().includes(search)
    );
  }
}
