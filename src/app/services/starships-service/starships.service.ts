import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Store } from '@ngxs/store';
import { SetStarShips } from '../../actions/starsips.actions';

@Injectable()
export class StarshipsService {
  menuTitle = 'Liste';
  constructor(private _httpClient: HttpClient, private _store: Store) {}

  async fetchStarShips() {
    const client_id = 'vyY86lvCUKn_FItWkxg2NO2Qpzaf-GHrPWh4fHP-Y4s';
    const images: any = await this._httpClient
      .get('https://api.unsplash.com/search/photos', {
        params: new HttpParams()
          .set('query', 'spaceship')
          .set('client_id', client_id),
      })
      .toPromise();
    const starShips: any = await this._httpClient
      .get(environment.apiUrl + 'starships')
      .toPromise();
    for (let index = 0; index < starShips.results.length; index++) {
      const starship = starShips.results[index];
      const pilots = [];
      for (let index = 0; index < starship.pilots.length; index++) {
        const pilot = await this._httpClient
          .get(starship.pilots[index])
          .toPromise();
        pilots.push({
          ...pilot,
          id: Number(
            starship.pilots[index]
              .split('/')
              .filter((value: any) => !isNaN(value) && !!value)[0]
          ),
        });
      }
      starShips.results[index].pilots = pilots;
      starShips.results[index] = {
        ...starShips.results[index],
        imageUrl: images.results[index]['urls'].regular,
        id: Number(
          starShips.results[index].url
            .split('/')
            .filter((value: any) => !isNaN(value) && !!value)[0]
        ),
      };
    }
    this._store.dispatch(new SetStarShips(starShips.results));
  }
}
